syntax on
colorscheme gruvbox
set number
set showmatch 
set colorcolumn=80
highlight colorcolumn ctermbg=darkgrey
set background=dark
set textwidth=80
set mouse=a
set spelllang=en_us
set tabstop=2
set cursorline
set norelativenumber
highlight CursorLineNr ctermfg=red
highlight LineNr cterm=bold ctermfg=darkgrey

set backup 
set writebackup 
